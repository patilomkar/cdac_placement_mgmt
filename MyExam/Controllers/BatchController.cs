﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyExam.Models;

namespace MyExam.Controllers
{
    public class BatchController : Controller
    {
        MyExamDBEntities daoObj = new MyExamDBEntities();

        // GET: Batch
        public ActionResult BatchList()
        {
            
            return View(daoObj.Batches.ToList());
        }

        public ActionResult Insert()
        {
            return View();

        }
        [HttpPost]
        public ActionResult Insert(Batch bat)
        {
            daoObj.Batches.Add(bat);
            daoObj.SaveChanges();
            return Redirect("/Batch/BatchList");

        }


    }
}