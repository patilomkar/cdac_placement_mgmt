﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;

namespace MyExam.Filters
{
    public class BatchFilter:ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string msgFormat = "{0}/{1} is Executing !!!";
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string method = filterContext.ActionDescriptor.ActionName;

            string msg = string.Format(msgFormat, controller, method);
            Debug.WriteLine(msg);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            string msgFormat = "{0}/{1} Executed";
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            string method = filterContext.ActionDescriptor.ActionName;

            string msg = string.Format(msgFormat, controller, method);
            Debug.WriteLine(msg); ;
        }
    }
}